
/* insert users */
INSERT INTO users
  (email,password,datetime_created)
VALUES("johnsmith@gmail.com", 'passwordA', '2021-01-01 01:00:00');

INSERT INTO users
  (email,password,datetime_created)
VALUES("juandelacruz@gmail.com", 'passwordB', '2021-01-01 02:00:00');


INSERT INTO users
  (email,password,datetime_created)
VALUES("junesmith@gmail.com", 'passwordC', '2021-01-01 03:00:00');

INSERT INTO users
  (email,password,datetime_created)
VALUES("mariadelacruz@gmail.com", 'passwordD', '2021-01-01 04:00:00');

INSERT INTO users
  (email,password,datetime_created)
VALUES("johndoe@gmail.com", 'passwordE', '2021-01-01 05:00:00');


/* insert user post */
INSERT INTO posts
  (title, content, author_id, datetime_posted)
VALUES("First Code", 'Hello World', (SELECT id
    FROM users
    WHERE email ='johnsmith@gmail.com'), "2021-01-02 01:00:00" );

INSERT INTO posts
  (title, content, author_id, datetime_posted)
VALUES("Second Code", 'Hello Earth!', (SELECT id
    FROM users
    WHERE email ='johnsmith@gmail.com'), "2021-01-02 02:00:00" );


INSERT INTO posts
  (title, content, author_id, datetime_posted)
VALUES("Third Code", 'Welcome to Mars!', (SELECT id
    FROM users
    WHERE email ='juandelacruz@gmail.com'), "2021-01-02 03:00:00" );


INSERT INTO posts
  (title, content, author_id, datetime_posted)
VALUES("Fourth Code", 'Bye bye solar system!', (SELECT id
    FROM users
    WHERE email ='mariadelacruz@gmail.com'), "2021-01-02 04:00:00" );


/* -- Selcting records */
SELECT *
FROM post
where author_id = 4
SELECT email, datetime_created
FROM users;

/* UPDATE records */
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

/* Deleting records */
DELETE FROM users WHERE email = "johndoe@gmail.com";